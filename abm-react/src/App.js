import React, { Component } from 'react';
import logo from './logo.svg';
import TablaPaises from './TablaPaises';
import './App.css';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import MisBadges from './MisBadges';
import CardSolcito from './CardSolcito';

class App extends Component {
  render() {
    return (
      <div className="App">
         
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>

        <h3>Países</h3>
        <TablaPaises recurso="paises"/>

        <h3>Pruebas de material-ui</h3>
        <MuiThemeProvider>
          <MisBadges cantidad={418}/>
          <CardSolcito />
        </MuiThemeProvider>

      </div>
    );
  }
}

export default App;
