import React from 'react';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';

const CardSolcito = () => (
  <Card>
    <CardHeader
      title="Solcito Perez"
      subtitle="Este es una hermosa en la que fui con mi novio"
      avatar="https://s3.amazonaws.com/arc-wordpress-client-uploads/infobae-wp/wp-content/uploads/2018/01/11155852/GENTE-SOL-PEREZ-10-110118.jpg"
    />

    <CardMedia overlay={<CardTitle title="Overlay title" subtitle="Overlay subtitle" />} >
      <img src="https://www.visittci.com/core/cover-resorts-boat-water-grace-bay-beach-providenciales_1024x341.jpg" alt="" />
    </CardMedia>

    <CardTitle title="Card title" subtitle="Card subtitle" />

    <CardText>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
      Donec mattis pretium massa. Aliquam erat volutpat. Nulla facilisi.
      Donec vulputate interdum sollicitudin. Nunc lacinia auctor quam sed pellentesque.
      Aliquam dui mauris, mattis quis lacus id, pellentesque lobortis odio.
    </CardText>

    <CardActions>
      <FlatButton label="Action1" />
      <FlatButton label="Action2" />
    </CardActions>
  </Card>
);

export default CardSolcito;
