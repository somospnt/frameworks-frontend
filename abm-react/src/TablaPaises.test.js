import React from 'react';
import renderer from 'react-test-renderer';
import TablaPaises from './TablaPaises.js';

test('TablaPaises se renderiza igual al último snapshot creado', () => {
  const component = renderer.create(
    <TablaPaises recurso="paises"/>
  );
var   tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
