class TablaPaises extends React.Component {

    constructor(props) {
        super(props);
        this.state = {paises: []};
        this.eliminar = this.eliminar.bind(this);
    }

    eliminar(pais) {
        this.setState(this.state.paises.splice(this.state.paises.indexOf(pais), 1));
    }

    componentDidMount() {
        $.get(`http://localhost:8080/api/${this.props.recurso}`)
                .then(respuesta => {
                    const paises = respuesta;
                    this.setState({paises});
                });
    }

    render() {
        return (
                <table className="table">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Superficie</th>
                            <th>Continente</th>
                            <th>Accion</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.paises.map(pais =>
                                        <tr key={pais.id}>
                                            <td>{pais.nombre}</td>
                                            <td>{pais.superficie}</td>
                                            <td>{pais.continente.nombre}</td>
                                            <td>
                                                <button
                                                    className="btn btn-sm"
                                                    onClick={() => this.eliminar(pais)}>
                                                    Eliminar
                                                </button>
                                            </td>
                                        </tr>
                                    )}
                    </tbody>
                </table>
                );
    }
}

ReactDOM.render(
        <TablaPaises recurso="paises"/>,
        document.getElementById('root')
        );
