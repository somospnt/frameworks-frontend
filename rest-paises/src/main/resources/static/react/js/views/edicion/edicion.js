function FormularioPais(props){
    
    function actualizar() {
        alert("llamado a función actualizar");
    }
    
    return (
            <div>
                <div class="col-xs-6">
                    <h3>Edición País</h3>
                    <div>
                        <form>
                            <Input nombre="Nombre" valorInicial={props.pais.nombre}/>
                            <Input nombre="Superficie" valorInicial={props.pais.superficie}/>
                            <div class="form-group">
                                <label htmlFor="continente">Continente</label>
                                <select class="form-control" id="continente">
                                    <option>Africa</option>
                                    <option>America</option>
                                    <option>Asia</option>
                                    <option defaultValue>Europa</option>
                                    <option>Oceania</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary" onClick={actualizar}>Actualizar</button>
                        </form>
                    </div>
                </div>
            </div>
    );
}

function Input(props){
    return (
        <div class="form-group">
            <label htmlFor="{props.nombre}">{props.nombre}</label>
            <input type="text" class="form-control" id="{props.nombre}" placeholder="{props.nombre}" defaultValue={props.valorInicial}/>
        </div>
    );
}

var paisMock = {
    nombre: "Inglaterra",
    superficie: "14654.50",
    continente: "Europa"
};

ReactDOM.render(
    <FormularioPais pais={paisMock} />,
    document.getElementById('root')
);