/* global app */

app.ui.views.index = (function () {

    function init() {
        renderizarItems();
    }

    function renderizarItems() {
        app.service.pais.obtenerTodos()
                .done(function (paisesResponse) {
                    var paises = paisesResponse._embedded.paises;
                    var tablaPersonasRowsHtml = $("#tabla-paises-rows-template").render(paises);
                    $('.pnt-js-tabla-paises').html(tablaPersonasRowsHtml);
                    bindearBotonEliminar();
                })
                .fail(function (xhr) {
                    alert("Error al consumir API Rest");
                });
    }

    function bindearBotonEliminar() {
        $('.pnt-js-eliminar-pais').on("click", function () {
            var id = $($(this).closest('tr')).data('id');
            app.service.pais.eliminar(id)
                    .done(renderizarItems)
                    .fail(function (xhr) {
                        alert("Error al consumir API Rest");
                    });
        });
    }

    return {
        init: init
    };

})();

$(document).ready(function () {
    app.ui.views.index.init();
});