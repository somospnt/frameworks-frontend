/* global app */

app.service.pais = (function () {

    function url() {
        return app.service.utils.url() + '/paises';
    }

    function obtenerTodos() {
        return app.service.utils.get(url());
    }

    function obtenerPorId(id) {
        return app.service.utils.get(url() + "/" + id);
    }

    function crear(pais) {
        return app.service.utils.post(url(), pais);
    }

    function eliminar(id) {
        return app.service.utils.eliminar(url() + "/" + id);
    }

    return {
        url: url,
        obtenerTodos: obtenerTodos,
        obtenerPorId: obtenerPorId,
        crear: crear,
        eliminar: eliminar
    };

})();