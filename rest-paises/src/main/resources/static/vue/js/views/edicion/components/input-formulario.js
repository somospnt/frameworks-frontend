/* global Vue, VueTimeago */

var inputFormularioComponent = Vue.component('input-formulario', {
    props: ['nombreLabel', 'nombreAtributo', 'objeto'], //parametros de entrada del componente
    template: '<div class="form-group">'
            + '<label v-bind:for="nombreLabel">{{nombreLabel}}</label>'
            + '<input type="text" class="form-control" v-bind:id="nombreLabel" v-bind:placeholder="nombreLabel" v-model="objeto[nombreAtributo]"/>'
            + '</div>'
});

