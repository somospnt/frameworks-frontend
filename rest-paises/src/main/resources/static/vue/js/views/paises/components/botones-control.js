/* global Vue */

var botonesControlComponent = Vue.component("botones-control", {
    template: '<div>'
            + '<a href="#">'
            + '<button v-on:click="eliminar(pais)" class="btn btn-sm pnt-js-eliminar-pais">Eliminar</button>'
            + '</a>'
            + '<a v-bind:href=" \'/vue/edicion/\' + pais.id">'
            + '<button class="btn btn-primary btn-sm pnt-js-eliminar-pais">Editar</button>'
            + '</a>'
            + '</div>',
    props: ['pais', 'paises'],
    methods: {
        eliminar: function (pais) {
            this.paises.splice(this.paises.indexOf(pais), 1);
        }
    }
});
