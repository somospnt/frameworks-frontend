/* global Vue, VueTimeago, service */

function actualizarPais() {
    alert("País actualizado");
}

var formularioPaisComponent = Vue.component('formulario-pais', {
    data () {
        return {
            pais: {},
            continentes: []
        };
    },
    created () {
        //POJS: Plain Old Javascript
        service.pais.obtenerPorId(1)
                .then(pais => {
                    this.pais = pais;
                });
        service.continente.obtenerTodos()
                .then(continente => {
                    this.continentes = continente;
                });
    },
    template: '<form>'
            + '<input-formulario nombre-label="Nombre" nombreAtributo="nombre" v-bind:objeto=pais></input-formulario>'
            + '<input-formulario nombreLabel="Superficie" nombreAtributo="superficie" v-bind:objeto=pais></input-formulario>'
            + '<div class="form-group">'
            + '<label for="continente">Continente</label>'
            + '<select class="form-control" id="continente" v-model="pais.continente.nombre">'
            + '<option v-for="continente in continentes">{{continente.nombre}}</option>'
            + ' </select>'
            + '</div>'
            + '<button type="submit" v-on:click="actualizarPais" class="btn btn-primary">Actualizar {{pais.nombre}}</button>'
            + ' </form>',
    methods: {
        actualizarPais: actualizarPais
    }
});
