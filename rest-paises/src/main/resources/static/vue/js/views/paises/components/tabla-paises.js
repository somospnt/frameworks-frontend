/* global Vue, service */

var tablaPaisesComponent = Vue.component('tabla-paises', {
    data() {
        return {paises: []};
    },
    created() {
        //POJS: Plain Old Javascript
        fetch(service.pais.url())
                .then(response => response.json())
                .then(json => {
                    this.paises = json;
                });
    },
    template: '<table class="table">'
            + '<thead>'
            + '<tr>'
            + '<th scope="col">Nombre</th>'
            + '<th scope="col">Superficie</th>'
            + '<th scope="col">Continente</th>'
            + '<th scope="col">Acción</th>'
            + '</tr>'
            + '</thead>'
            + '<tbody class="pnt-js-tabla-paises">'
            + '<tr v-for="pais in paises">'
            + '<td>'
            + '{{pais.nombre}}'
            + '</td>'
            + '<td>'
            + '{{pais.superficie}}'
            + '</td>'
            + '<td>'
            + '{{pais.continente.nombre}}'
            + '</td>'
            + '<td>'
            + '<botones-control v-bind:pais="pais" v-bind:paises="paises"></botones-control>'
            + '</td>'
            + '</tr>'
            + '</tbody>'
            + '</table>'
});
