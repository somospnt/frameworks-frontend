/* global app */
var service = service || {};

service.utils = (function () {
    function url() {
        return 'http://localhost:8080/api';
    }

    function get(url) {
        return $.ajax({
            type: 'GET',
            url: url,
            contentType: 'application/json; charset=UTF-8',
            dataType: 'json'
        });
    }

    function post(url, data) {
        return $.ajax({
            type: 'POST',
            url: url,
            contentType: 'application/json; charset=UTF-8',
            dataType: 'json',
            data: JSON.stringify(data)
        });
    }

    function put(url, data) {
        return $.ajax({
            type: 'PUT',
            url: url,
            contentType: 'application/json; charset=UTF-8',
            dataType: 'json',
            data: JSON.stringify(data)
        });
    }

    function eliminar(url, data) {
        return $.ajax({
            type: 'DELETE',
            url: url,
            dataType: 'json'
        });
    }

    return {
        url: url,
        get: get,
        post: post,
        put: put,
        eliminar: eliminar
    };
})();