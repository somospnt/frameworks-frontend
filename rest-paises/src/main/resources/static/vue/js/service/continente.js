/* global app */
var service = service || {};

service.continente = (function () {

    function url() {
        return service.utils.url() + '/continentes';
    }

    function obtenerTodos() {
        return service.utils.get(url());
    }

    function obtenerPorId(id) {
        return service.utils.get(url() + "/" + id);
    }

    function crear(pais) {
        return service.utils.post(url(), pais);
    }

    function eliminar(id) {
        return service.utils.eliminar(url() + "/" + id);
    }

    return {
        url: url,
        obtenerTodos: obtenerTodos,
        obtenerPorId: obtenerPorId,
        crear: crear,
        eliminar: eliminar
    };

})();