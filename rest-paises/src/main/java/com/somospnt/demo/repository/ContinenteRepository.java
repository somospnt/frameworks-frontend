package com.somospnt.demo.repository;

import com.somospnt.demo.domain.Continente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContinenteRepository extends JpaRepository<Continente, Long>{

}
