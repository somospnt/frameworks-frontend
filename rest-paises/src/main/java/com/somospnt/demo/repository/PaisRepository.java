package com.somospnt.demo.repository;

import com.somospnt.demo.domain.Pais;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaisRepository extends JpaRepository<Pais, Long>{

}
