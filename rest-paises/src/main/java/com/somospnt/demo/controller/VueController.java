package com.somospnt.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/vue")
public class VueController {

    @GetMapping
    public String index() {
        return "vue/routing";
    }

    @GetMapping("/paises")
    public String paises() {
        return "vue/paises";
    }

    @GetMapping("/edicion/{id}")
    public String edicion(@PathVariable("id") Long id) {
        return "vue/edicion";
    }

}
