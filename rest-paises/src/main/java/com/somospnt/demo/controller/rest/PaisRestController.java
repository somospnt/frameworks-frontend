package com.somospnt.demo.controller.rest;

import com.somospnt.demo.domain.Pais;
import com.somospnt.demo.repository.PaisRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/paises")
@CrossOrigin
public class PaisRestController {

    @Autowired
    private PaisRepository paisRepository;
    
    @GetMapping
    public List<Pais> obtenerTodos(){
        return paisRepository.findAll();
    }
    
    @GetMapping("/{id}")
    public Pais obtener(@PathVariable Long id){
        return paisRepository.findOne(id);
    }
    
    @PutMapping("/{id}")
    public Pais actualizar(@PathVariable Long id, @RequestBody Pais pais){
        return paisRepository.saveAndFlush(pais);
    }
}
