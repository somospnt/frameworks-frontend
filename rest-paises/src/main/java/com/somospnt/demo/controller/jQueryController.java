package com.somospnt.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/jquery")
public class jQueryController {

    @GetMapping("/paises")
    public String index() {
        return "jquery/paises";
    }

    @GetMapping("/alta")
    public String alta() {
        return "jquery/alta";
    }

    @GetMapping("/edicion/{id}")
    public String edicion(@PathVariable("id") Long id) {
        return "jquery/edicion";
    }

}
