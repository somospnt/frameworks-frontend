package com.somospnt.demo.controller.rest;

import com.somospnt.demo.domain.Continente;
import com.somospnt.demo.repository.ContinenteRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/continentes")
@CrossOrigin
public class ContinenteRestController {

    @Autowired
    private ContinenteRepository continenteRepository;

    @GetMapping
    public List<Continente> obtenerTodos() {
        return continenteRepository.findAll();
    }

    @GetMapping("/{id}")
    public Continente obtener(@PathVariable Long id) {
        return continenteRepository.findOne(id);
    }

}
