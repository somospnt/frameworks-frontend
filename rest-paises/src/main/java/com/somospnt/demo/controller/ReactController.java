package com.somospnt.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/react")
public class ReactController {

    @GetMapping("/paises")
    public String listado() {
        return "react/paises";
    }

    @GetMapping("/edicion")
    public String edicion() {
        return "react/edicion";
    }

}
