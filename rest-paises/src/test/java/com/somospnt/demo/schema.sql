CREATE TABLE pais (
    id serial PRIMARY KEY,
    nombre VARCHAR (255),
    superficie decimal,
    continente BIGINT
);

CREATE TABLE continente (
    id serial PRIMARY KEY,
    nombre VARCHAR (255)
);

