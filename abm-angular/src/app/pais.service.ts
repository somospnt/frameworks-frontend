import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { Pais } from './pais';

@Injectable()
export class PaisService {

  private paisesUrl = 'http://localhost:8080/api/paises';  // URL to web api

  constructor(private http: HttpClient) { }

  getPais (): Observable<Pais> {
    return this.http.get<Pais>(this.paisesUrl + '/' + 2);

  }

  updatePais(pais: Pais): Observable<any> {
    return this.http.put<Pais>(this.paisesUrl + '/' + pais.id, pais);
  }

  getPaises(): Observable<any> {
    return this.http.get<any>(this.paisesUrl)
      .pipe(
        tap(paises => console.log(`paises: ` + paises))
      );
  }
  
}
