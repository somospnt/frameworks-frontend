import { Component, OnInit, Input } from '@angular/core';

import { Pais }         from '../pais';
import { PaisService }  from '../pais.service';

@Component({
  selector: 'pais-details',
  templateUrl: './pais.component.html',
  styleUrls: ['./pais.component.css']
})
export class PaisComponent implements OnInit{
  @Input() pais: Pais;

  constructor(
    private paisService: PaisService
  ) {}


  ngOnInit(): void {
  this.getPais();
  }

  getPais(): void {
      console.log("get");
      this.paisService.getPais()
      .subscribe(pais => {
        console.log("entró");
        this.pais = pais;
      });
  }

  updatePais(pais: Pais): void {
    console.log("update");
    this.paisService.updatePais(pais)
    .subscribe(_ => {
      console.log("entró");
    });
  }

}
