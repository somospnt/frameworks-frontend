import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule }    from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { PaisComponent } from './pais/pais.component';
import { PaisesComponent } from './paises/paises.component';

import { PaisService } from './pais.service';

const appRoutes: Routes = [
  { path: 'edicion', component: PaisComponent },
  { path: 'listado', component: PaisesComponent },
];

@NgModule({ //toda la anotación es un decorator
  declarations: [ //clases de vista
    AppComponent,
    PaisComponent,
    PaisesComponent
  ],
  imports: [ // todo lo que van a usar los componentes del modulo
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  exports: [ RouterModule ], //lo que ven desde componentes externos
  providers: [ PaisService ], //services globales
  bootstrap: [ AppComponent ] //solo para modulo principal
})
export class AppModule { }
