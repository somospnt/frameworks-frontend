import { Component ,OnInit} from '@angular/core';

import { Pais } from './../pais';
import { PaisService } from './../pais.service';

@Component({
  selector: 'paises',
  templateUrl: './paises.component.html'
})
export class PaisesComponent implements OnInit {
  paises: Pais[];

  constructor(private paisService: PaisService) { }

  ngOnInit() {
    this.getPaises();
  }

  getPaises(): void {
    this.paisService.getPaises()
    .subscribe(paises => this.paises = paises);
  }

  delete(pais: Pais): void {
    this.paises = this.paises.filter(h => h !== pais);
  }

}
