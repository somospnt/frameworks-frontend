import { Continente } from './continente';

export class Pais {
  id: number;
  nombre: string;
  continente: Continente;
  superficie: number;
}
